package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/myapp/model"
	"myapp/myapp/utils/httpResp"
	"myapp/myapp/utils/serial"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

func CreateTravel(w http.ResponseWriter, r *http.Request) {
	var travel model.Travel
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&travel); err != nil {
			httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
			return
		}
		travel.SerialNo = serial.GenerateSerialNumber()
		defer r.Body.Close()

		saveErr := travel.Create()
		if saveErr != nil {
			if strings.Contains(saveErr.Error(), "duplicate key") {
				httpResp.RespondWithError(w, http.StatusForbidden, "Duplicate keys")
			return
		} else {
			httpResp.RespondWithError(w, http.StatusInternalServerError, saveErr.Error())
		}
	}
	// no error
	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"status": "travel list added"})
}

// func CreateTravel(w http.ResponseWriter, r *http.Request) {
// 	var travel model.Travel
// 	decoder := json.NewDecoder(r.Body)
// 	if err := decoder.Decode(&travel); err != nil {
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid JSON body")
// 		return
// 	}
// 	defer r.Body.Close()

// 	// Process the uploaded file
// 	file, _, err := r.FormFile("image")
// 	if err != nil {
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "failed to retrieve image from the request")
// 		return
// 	}
// 	defer file.Close()

// 	travel.SerialNo = serial.GenerateSerialNumber()
// 	travel.Image = file
	
// 	// travel.Description = "..." // Set the description from the request or leave as-is

// 	saveErr := travel.Create()
// 	if saveErr != nil {
// 		if strings.Contains(saveErr.Error(), "duplicate key") {
// 			httpResp.RespondWithError(w, http.StatusForbidden, "Duplicate keys")
// 		} else {
// 			httpResp.RespondWithError(w, http.StatusInternalServerError, saveErr.Error())
// 		}
// 		return
// 	}

// 	// No error
// 	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"status": "travel list added"})
// }



func GetATList(w http.ResponseWriter, r *http.Request){
	// if !VerifyCookie(w,r){
	// 	return 
	// }
	//get url parameter
	sn := mux.Vars(r)["serialNo"]
	travel := model.Travel{SerialNo: sn}
	// fmt.Println(travel)
	getErr := travel.Get()
	if getErr != nil{
		switch getErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Travel List not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	
	} else{
		httpResp.RespondWithJson(w, http.StatusOK, travel)
	}
}
func UpdateTList(w http.ResponseWriter, r *http.Request){
	// if !VerifyCookie(w,r){
	// 	return 
	// }
	old_sn := mux.Vars(r)["serialNo"]
	// old_stdId:= getUserId(old_sid)
	//student instance
	var travel model.Travel
	//json object
	decoder := json.NewDecoder(r.Body)
	
	if err:= decoder.Decode(&travel); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	getErr := travel.Update(old_sn)
	if getErr != nil{
		switch getErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "list not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	
	} else{
		httpResp.RespondWithJson(w, http.StatusOK, travel)
	}
}

func DeleteTList(w http.ResponseWriter, r *http.Request){
	// if !VerifyCookie(w,r){
	// 	return 
	// }
	sn := mux.Vars(r)["serialNo"]
	// stdId:= getUserId(sid)
	s := model.Travel{SerialNo: sn}
	//student instance
	if err:= s.Delete(); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status":"deleted"})
}


func GetTList (w http.ResponseWriter, r *http.Request){
	enrolls, getErr := model.GetAllTravel()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, enrolls)

}