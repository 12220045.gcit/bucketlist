package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

//db info

const(
	postgres_host = "db"
	postgres_port = 5432
	postgres_user = "postgres"
	postgres_password = "postgres"
	postgres_dbname = "bucketlist_db"
)

var Db *sql.DB

func init(){
	db_info := fmt.Sprintf("host= %s port=%d user=%s password=%s dbname=%s sslmode=disable", postgres_host, postgres_port,postgres_user,postgres_password, postgres_dbname)
	var err error
	Db, err = sql.Open("postgres", db_info)
	if err != nil{
		log.Println(err)
	} else {
		fmt.Println("Database successfully connected")
	}
}