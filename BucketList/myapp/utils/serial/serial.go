package serial

import (
	"fmt"
)

// func GenerateSerialNumber() string {
// 	serial := xid.New()
// 	return serial.String()
// }
var (
	serialNumber int
)
func init() {
	serialNumber = 0 // Start with a specific initial serial number
}

func GenerateSerialNumber() string {
	serialNumber++
	// rand.Seed(time.Now().UnixNano())

	// Generate a random number between 100000 and 999999
	// serialNumber := rand.Intn(90) + 10

	return fmt.Sprintf("%d", serialNumber)
}