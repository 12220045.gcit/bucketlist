function logout(){
    fetch('/logout')
    .then(response => {
        if (response.ok){
            document.cookie = ""
            window.open("index.html", "_self")
            console.log("cookie unset")
        }
        else{
            throw new Error(response.statusText)
        }
    })
    .catch(e => {
        alert(e)
    })
}