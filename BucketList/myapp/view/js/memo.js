///new code
function removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
  }
  
  $('.image-upload-wrap').bind('dragover', function () {
    $('.image-upload-wrap').addClass('image-dropping');
  });
  
  $('.image-upload-wrap').bind('dragleave', function () {
    $('.image-upload-wrap').removeClass('image-dropping');
  });
  
  function createNewDiv(imageUrl) {
    // Get the user input values
    var text = document.getElementById("text").value;
  
    // Create a new div element
    var newDiv = document.createElement("div");
  
    // Set some attributes for the new div
    newDiv.className = "new-class";
  
    // Create a new image element
    var image = document.createElement("img");
  
    // Set the source attribute for the image
    image.src = imageUrl;
  
    // Create a new text element
    var textNode = document.createTextNode(text);
  
    // Append the image and text elements to the new div
    newDiv.appendChild(image);
    newDiv.appendChild(textNode);
  
    // Append the new div to the container element
    var container = document.getElementById("container");
    container.appendChild(newDiv);
  }
  var reader;
  const submit = document.getElementById("submit")
  
  submit.addEventListener('click',(e)=>{
    e.preventDefault();
    addTravel();
  })
  document.querySelector("#imageUrl").addEventListener("change", function(){
  
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("image-dataurl", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
  })
  
  
  async function addTravel(){
    var data = {
        Title:document.getElementById("textInput").value,
        Image:localStorage.getItem("image-dataurl"),
        Description:document.getElementById('text').value
    }
  
    // console.log(data)
  
    fetch("/travel",{
        method: "POST",
        body : JSON.stringify(data),
        headers :{"Content-type" : "application/json; charset = UTF-8"}
  
    })
    setTimeout(function() {
      location.reload();
    }, 2000);
  }
  
  function readURL(input) {
    if (input.files && input.files[0]) {
  
      reader = new FileReader();
  
      reader.onload = function(e) {
        $('.image-upload-wrap').hide();
  
        $('.file-upload-image').attr('src', e.target.result);
        $('.file-upload-content').show();
  
        $('.image-title').html(input.files[0].name);
  
        // createNewDiv(reader.result);
      };
  
      reader.readAsDataURL(input.files[0]);
  
    } else {
      removeUpload();
    }
  }
  
  
  
  
  
  window.onload = function() {
    // Backtrack()
  fetch('/travels')
      .then(response => response.text())
      .then(data => {
          // console.log(data);
          getAllTravel(data);
      })
      .catch(error => {
          console.error('Error fetching data:', error);
      });
  };
  
  async function getAllTravel(data){
    const travels = JSON.parse(data);
    // console.log(data)
    travels.forEach(travel=>{
      var container = document.getElementById('posts');
      var div1 =document.createElement('div')
      div1.className = 'post'
      container.appendChild(div1)
  
      var leftDiv = document.createElement('div')
      leftDiv.className = 'left'
      div1.appendChild(leftDiv)
  
      var serialno = document.createElement('p')
      serialno.className = 'serialno'
      serialno.innerHTML = `Serial No: `+travel.SerialNo
      leftDiv.appendChild(serialno)
  
      var img = document.createElement('img')
      img.className = 'img-cont'
      img.setAttribute('src',travel.Image)
      leftDiv.appendChild(img)
  
      var rightDiv = document.createElement('div')
      rightDiv.className = 'right'
      div1.appendChild(rightDiv)
  
      var h3 = document.createElement('h3');
      h3.className = 'title'
      h3.innerHTML = travel.Title
      rightDiv.appendChild(h3)
  
      var description = document.createElement('p')
      description.className = 'description'
      description.innerHTML = travel.Description
      rightDiv.appendChild(description)
  
      var btnDiv = document.createElement('div')
      btnDiv.className = "btn"
      div1.appendChild(btnDiv)
  
      var deleteBtn = document.createElement("div");
      deleteBtn.className = "deleteBtn"
      deleteBtn.id = "delete"
      deleteBtn.innerHTML = "DELETE POST"
      btnDiv.onclick = deletePost
      btnDiv.appendChild(deleteBtn)
    })
  }
  
  
  function deletePost(event) {
    var deleteBtn = event.target;
    var postContainer = deleteBtn.parentNode.parentNode;
    var serialNoElement = postContainer.querySelector('.serialno');
    var serialNo = serialNoElement.textContent.replace('Serial No: ', '');
  
    if (confirm("Are you sure you want to delete this?")) {
      fetch('/travel/' + serialNo, {
        method: "DELETE",
        headers: { "Content-type": "application/json; charset=UTF-8" }
      })
      .then(response => {
        if (response.ok) {
          postContainer.parentNode.removeChild(postContainer);
        } else {
          console.error('Error deleting post:', response.statusText);
        }
      })
      .catch(error => {
        console.error('Error deleting post:', error);
      });
    }
  }
  
  
  
  // var btn = document.getElementById("delete")
  
  // btn.addEventListener('click',function() {
  //   if (confirm("Are you sure you want to DELETE this?")){
  //     var parentNode = deleteButton.parentNode;
  //     parentNode.parentNode.removeChild(parentNode);
  //   }
  // })