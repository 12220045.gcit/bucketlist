// angular.module('userProfile', ['ngMaterial', 'ngMessages'])
//   .controller('Ctrl', function($scope) {
//     $scope.viewMode = true;
  
//     // Switch between view mode and edit mode
//     $scope.switchMode = function() {
//       return $scope.viewMode = !$scope.viewMode;
//     };
  
//     // Save the changes
//     $scope.saveChanges = function() {
//       /*Validate the input
//       Save the changes*/
//     };
  
//     // User data
//     $scope.user = {
//       name: 'Adam Luxor',
//       title: 'Ama',
//       department: 'Eating',
//       location: 'Atlanta',
//       company: 'Transgender',
//       manager: '12/12/2012',
//       workPhone: 'Gyalpozhing',
//       mobilePhone: '17644nepal7',
//       alias: 'I am...... from ......',
//       otherEmail: 'adaml@amaagmail.com'
//     }
//   });

  
  // New

//   window.onload = function() {

//     fetch('/signups')
//       .then(response => response.json()) // Parse the response as JSON
//       .then(data => {
//         console.log(data);
//         GetUser(data);
//       })
//       .catch(error => {
//         console.error('Error fetching data:', error);
//       });
//   };
//   async function GetUser(data){
//     const users = JSON.parse(data)

//     users.forEach(user =>{
//         const usersession = JSON.parse( sessionStorage.getItem('user'))
//         var name = document.getElementById("username")
//         var email = document.getElementById("email")
   
//         if(usersession.Email == user.Email){
//             name.innerHTML = usersession.Name
//             email.innerHTML = usersession.Email
//         }

//         document.querySelector('.close').addEventListener("click", function(){
//             document.querySelector('.content_table').style.display = 'none'
//         })
        
//         document.getElementById("editButton").addEventListener('click', function(){
//             document.querySelector('.content_table').style.display = 'flex'
//         })
//         const button = document.getElementById("editButton")
        
//         button.addEventListener('click', editProfile(this))
//     })
// }


// const editProfile = async() =>{

//   const user = JSON.parse( sessionStorage.getItem('user'))

//   document.getElementById('name').value = user.Username
//   document.getElementById('email1').value = user.Email
//   document.getElementById('password').value = user.Password

 
//   var btn = document.getElementById("upButton");
//   if(btn){
//       btn.setAttribute("onclick", "update()");
//   }

// }


// const update = async () =>{
//   if (document.getElementById('password').value == document.getElementById('cpassword').value){
     
//       var newdata = {
//           Username:document.getElementById('name').value,
//           Email:document.getElementById('email1').value,
//           Password : document.getElementById('cpassword').value
      
//       }

//       const userSession = JSON.parse( sessionStorage.getItem('user'))
  
//       alert(newdata.Username + newdata.Email + newdata.Password)

//       fetch("/signup/"+userSession.Email,{
//           method:"PUT",
//           body:JSON.stringify(newdata),
//           headers:{"Content-type":"application/json; charset=UTF-8"}
//       });

      
//       sessionStorage.setItem('user', JSON.stringify(newdata))
//       resetForm();
//   }
//   else{
//       document.querySelector('.validation').style.display = 'block'
//   }
// }

// function resetForm(){
//   document.getElementById('name').value = "";
//   document.getElementById('email1').value = "";
//   document.getElementById('password').value = "";
//   document.getElementById('cpassword').value = "";
// }

window.onload = function() {
    Backtrack()
  fetch('/signups')
      .then(response => response.text())
      .then(data => {
          console.log(data);
          GetUser(data);
      })
      .catch(error => {
          console.error('Error fetching data:', error);
      });
};


function Backtrack(){
    
    const usersession = sessionStorage.getItem('user')

    console.log(usersession)
    if (usersession == null){
        alert('NaNaNa')
        window.location.href = "index.html"
    }
}



const data = sessionStorage.getItem('user')


async function GetUser(data) {
  const users = JSON.parse(data)

  users.forEach(user => {
      const userSession = JSON.parse(sessionStorage.getItem('user'));
      var name = document.getElementById("username");
      var email = document.getElementById("email");

      if (userSession.Email === user.Email) {
          name.innerHTML = userSession.Name;
          email.innerHTML = userSession.Email;
      }

      document.querySelector('.close').addEventListener("click", function() {
          document.querySelector('.content_table').style.display = 'none';
      });

      document.getElementById("editButton").addEventListener('click', function() {
          document.querySelector('.content_table').style.display = 'flex';
      });

      const button = document.getElementById("editButton");

      button.addEventListener('click', editProfile);
  });
}
  

const editProfile = async () => {
//   const user = JSON.parse(sessionStorage.getItem('user'));
  // Usage:
  const user = JSON.parse( sessionStorage.getItem('user'))
//   console.log(user)
  document.getElementById('name').value = user.Name;
  document.getElementById('email1').value = user.Email;
  document.getElementById('password').value = user.Password;

  var btn = document.getElementById("upButton");
  if (btn) {
      btn.setAttribute("onclick", "update()");
  }
};

const update = async () => {
  if (document.getElementById('password').value === document.getElementById('cpassword').value) {
      var newdata = {
          Name: document.getElementById('name').value,
          Email: document.getElementById('email1').value,
          Password: document.getElementById('cpassword').value
      };

      const userSession = JSON.parse(sessionStorage.getItem('user'));

    //   console.log(newdata.Name, newdata.Email, newdata.Password);

      await fetch("/signup/" + userSession.Email, {
          method: "PUT",
          body: JSON.stringify(newdata),
          headers: {"Content-type": "application/json; charset=UTF-8"}
      });

      sessionStorage.setItem('user', JSON.stringify(newdata));
      resetForm();
  } else {
      document.querySelector('.validation').style.display = 'block';
  }

  return false;
};

function resetForm() {
  document.getElementById('name').value = "";
  document.getElementById('email1').value = "";
  document.getElementById('password').value = "";
  document.getElementById('cpassword').value = "";
}
