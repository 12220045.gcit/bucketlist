package model

import "myapp/myapp/datastore/postgres"



type Bucket struct {
	SerialNo    string `json:"serialNo"`
	Name        string `json:"name"`
	Date        string `json:"date"`
	Description string `json:"description"`
	Categories  string `json:"categories"`
	Location    string `json:"location"`
	Status      string `json:"status"`
}

const queryInsertBucket = " INSERT INTO bucket(serialNo, Name, Date, Description, Categories, Location, Status) VALUES($1,$2,$3, $4, $5,$6, $7);"
const queryGetBucket = "SELECT serialNo, Name, Date, Description, Categories, Location, Status FROM bucket WHERE serialNo=$1;"

const (
	// queryGetUser    = "Select name,email,password from admin where email=$1"
	queryDeleteBucket = "DELETE from bucket where serialNo= $1;"
	queryUpdateBucket = "UPDATE bucket SET serialNo=$1,Name=$2, Categories=$3, Date=$4,Location=$5, Description=$6, Status=$7 where serialNo=$8;"
)


func (b *Bucket) Create() error {
	_, err := postgres.Db.Exec(queryInsertBucket,  b.SerialNo, b.Name, b.Date, b.Description, b.Categories, b.Location, b.Status)
	return err
}
func (b *Bucket) Get() error{
	return postgres.Db.QueryRow(queryGetBucket, b.SerialNo).Scan(&b.SerialNo, &b.Name, &b.Date, &b.Description,  &b.Categories, &b.Location, &b.Status)
}



func GetAllList()([]Bucket, error){
	rows, getErr := postgres.Db.Query("SELECT serialNo, Name, Date, Description, Categories, Location, Status FROM bucket")
	if getErr != nil{
		return nil, getErr
	}
	//create a slice of type Course
	enrolls:= []Bucket{}

	for rows.Next(){
		var e Bucket
		dbErr := rows.Scan(&e.SerialNo, &e.Name,&e.Date, &e.Description, &e.Categories, &e.Location, &e.Status)
		if dbErr != nil{
			return nil, dbErr
		}
		enrolls = append(enrolls, e)

	}
	rows.Close()
	return enrolls, nil
}

func (b *Bucket)Delete() error{
	if _, err:= postgres.Db.Exec( queryDeleteBucket,  b.SerialNo); err != nil{
		return err
	}
	return nil
}

func (b *Bucket) Update(old_sn string) error {
	_,err := postgres.Db.Exec(queryUpdateBucket, b.SerialNo, b.Name, b.Date, b.Description, b.Categories, b.Location, b.Status, old_sn)
	return err
}

